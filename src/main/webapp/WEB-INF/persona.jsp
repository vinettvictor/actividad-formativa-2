<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>    
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Persona</title>
</head>
<h1>Crear Persona</h1>
<body>
	<div>
		<form:form action="/persona/crear" method="post" modelAttribute="persona">
			<form:label path="rut">Rut</form:label>
			<form:input type="text" path="rut"></form:input><br>
			
			<form:label path="nombre">Nombre</form:label>
			<form:input type="text" path="nombre"></form:input><br>
			
			<form:label path="apellido">Apellido</form:label>
			<form:input type="text" path="apellido"></form:input><br>
			
			<form:label path="email">Correo</form:label>
			<form:input type="text" path="email"></form:input><br>
			
			<input type="submit" value="Crear Persona">
		</form:form>
		<br>
		<h1>Lista Persona</h1>
		<hr>
		<table>
			<thead>
				<tr>
					<th>Id</th>
					<th>Rut</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>email</th>

				</tr>
			</thead>
			<tbody>
				<c:forEach var="persona" items="${lista_personas}" >
					<tr>
						<td><c:out value="${persona.id}"></c:out></td>
						<td><c:out value="${persona.rut}"></c:out></td>
						<td><c:out value="${persona.nombre}"></c:out></td>
						<td><c:out value="${persona.apellido}"></c:out></td>
						<td><c:out value="${persona.email}"></c:out></td>
						<td> 
							<a href="/persona/actualizar/${persona.id}">Editar</a>
							
							<form action="/persona/eliminar/${persona.id}" method="post">
								<input type="hidden" name="_method" value="delete">
								<input type="submit" value="Eliminar">
							</form>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<hr>
	</div>
</body>
</html>