package com.everis.data.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.everis.data.models.Persona;
import com.everis.data.services.PersonaService;

@Controller
@RequestMapping("/persona")
public class PersonaController {
	
	private final PersonaService personaService;
	public PersonaController(PersonaService personaService) {
		this.personaService = personaService;
	}
		
	@RequestMapping("")
	public String index(@ModelAttribute("persona") Persona persona, Model model) {
		
		List<Persona> lista_personas = personaService.findAll();
		model.addAttribute("lista_personas",lista_personas);
		return "persona.jsp";
	}
	
	//METODO PARA INSERTAR DATOS
		@RequestMapping(value="/crear", method = RequestMethod.POST)
		public String crearPersona(@Valid @ModelAttribute("persona") Persona persona) {			
				//llamado a guardar objeto a la bd	
			
				if(!persona.getNombre().isBlank() && !persona.getApellido().isBlank() && !persona.getRut().isBlank() && !persona.getEmail().isBlank()) {
					if(persona.getNombre().length() <3 || persona.getNombre().length() > 20) {
						System.out.println("error al ingresar nombre (mínimo 3 y maximo 20)");						
					}else if (persona.getApellido().length() < 3 || persona.getApellido().length() >20 ){
						System.out.println("error al ingresar apellido (mínimo 3 y maximo 20)");					
					} else {
						personaService.crearPersona(persona); 
						System.out.println("Persona creado:  " +persona.getRut());
					}
				}else {
					
					System.out.println("Por favor ingrese valores");
				}			
			return "redirect:/persona"; //redirect: enviarme a alguna ruta que queremos mostrar
		}
		
		//METODO PARA ACTUALIZAR
		@RequestMapping(value="/actualizar/{id}", method = RequestMethod.GET)
		public String actualizarPersona(@PathVariable("id") Long id, Model model) {
			System.out.println("El id a actualizar es: "+id);
			Persona persona = personaService.buscarPersona(id);
			model.addAttribute("persona", persona);
			return "editarPersona.jsp";
		}
		
		
		//MÉTODO CREADO PARA MODIFICAR PERSONA
		@RequestMapping(value = "/modificar", method = RequestMethod.PUT)
		public String modificar(@Valid @ModelAttribute("persona") Persona persona) {
			System.out.println("id a modificar: " + persona.getId());
			
			//Validamos que no sea nulo los datos, y cumplan los requisitos de nombre y apellido
			if(!persona.getNombre().isBlank() && !persona.getApellido().isBlank() && !persona.getRut().isBlank() && !persona.getEmail().isBlank()) {
				if(persona.getNombre().length() <3 || persona.getNombre().length() > 20) {
					System.out.println("error al ingresar nombre (mínimo 3 y maximo 20)");
				}else if (persona.getApellido().length() < 3 || persona.getApellido().length() >20 ){
					System.out.println("error al ingresar apellido (mínimo 3 y maximo 20)");
				} else {
					personaService.crearPersona(persona); 
					System.out.println("Persona creado:  " +persona.getRut());
					return "redirect:/persona";
				}
			}else {
				System.out.println("Por favor ingrese valores");
			}				
			return "redirect:/persona/actualizar/"+persona.getId();
		}
		
		
		//METODO PARA ELIMINAR
		@RequestMapping(value="/eliminar/{id}", method = RequestMethod.DELETE)
		public String eliminarPersona(@PathVariable("id") Long id) {
			System.out.println("El id a eliminar es: "+id);
			personaService.eliminarPersona(id);
			return "redirect:/persona";
		}
	
	
}
