package com.everis.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActividadFormativa212Application {

	public static void main(String[] args) {
		SpringApplication.run(ActividadFormativa212Application.class, args);
	}

}
