package com.everis.data.services;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.everis.data.models.Persona;
import com.everis.data.repositories.PersonaRepository;

@Service
public class PersonaService {

	private final  PersonaRepository personaRepository;
	public PersonaService(PersonaRepository personaRepository) {
		this.personaRepository = personaRepository; 
	}
	
	public void crearPersona(@Valid Persona persona) {			
		personaRepository.save(persona);
	}
	
	public List<Persona> findAll() {
		return personaRepository.findAll();
	}
	public void eliminarPersona(Long id) {
		personaRepository.deleteById(id);		
	}
	public Persona buscarPersona(Long id) {
		Optional<Persona> oPersona = personaRepository.findById(id);
		if(oPersona.isPresent()) {
			return oPersona.get();
		}
		return null;
	}
	public void modificarPersona(@Valid Persona persona) {	
		personaRepository.save(persona);
	}

}
